#include "testrdadevicepropertyaccess.h"

void TestRdaDevicePropertyAccess::initTestCase()
{
     qDebug("called before everything else");
}

void TestRdaDevicePropertyAccess::subscriptionTest()
{
      QString devicePropertyName = "INCA.TESTDEV-01-001-F3/SettingsControl";
      QString selector = "PSB.USER.SFTPRO1";
      QString devicePropertyNameSelector = selector + ":" + devicePropertyName;
      QStringList dataKeys({devicePropertyNameSelector + "#boolCtrl", devicePropertyNameSelector + "#doubleArray2DCtrl", devicePropertyNameSelector + "#doubleArrayCtrl", devicePropertyNameSelector + "#floatArray2DCtrl", devicePropertyNameSelector + "#floatArrayCtrl", devicePropertyNameSelector + "#floatCtrl", devicePropertyNameSelector + "#floatCtrl_max", devicePropertyNameSelector + "#floatCtrl_min", devicePropertyNameSelector + "#floatCtrl_units", devicePropertyNameSelector + "#int16Ctrl", devicePropertyNameSelector + "#int16Ctrl_max", devicePropertyNameSelector + "#int16Ctrl_min", devicePropertyNameSelector + "#int16Ctrl_units", devicePropertyNameSelector + "#int32ArrayCtrl", devicePropertyNameSelector + "#int8Array2DCtrl", devicePropertyNameSelector + "#stringArrayCtrl", devicePropertyNameSelector + "#stringCtrl"});
      RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
      rdaDevicePropertyAccess_->subscription(devicePropertyName,selector,"");

      QQmlPropertyMap * data = rdaDevicePropertyAccess_->getRdaDataAcquired();

      //Wait for data to be filled
      QTest::qSleep(20);

      QVERIFY(data->keys() == dataKeys);
      QVERIFY(rdaDevicePropertyAccess_->subscription_.size() == 1);
      QVERIFY(rdaDevicePropertyAccess_->subscription_.contains(devicePropertyNameSelector));
}

void TestRdaDevicePropertyAccess::unsubscriptionTest()
{
      QString devicePropertyName = "INCA.TESTDEV-01-001-F3/SettingsControl";
      QString selector = "PSB.USER.SFTPRO1";
      QString devicePropertyNameSelector = selector + ":" + devicePropertyName;
      RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
      rdaDevicePropertyAccess_->subscription(devicePropertyName,selector,"");

      QVERIFY(rdaDevicePropertyAccess_->subscription_.size() == 1);
      QVERIFY(rdaDevicePropertyAccess_->subscription_.contains(devicePropertyNameSelector));

      rdaDevicePropertyAccess_->unsubscription(devicePropertyName, selector);

      QVERIFY(rdaDevicePropertyAccess_->subscription_.size() == 0);
}


void TestRdaDevicePropertyAccess::getTest()
{
      QString devicePropertyName = "INCA.TESTDEV-01-001-F3/SettingsControl";
      QString selector = "PSB.USER.SFTPRO1";
      QString devicePropertyNameSelector = selector + ":" + devicePropertyName;
      QStringList dataKeys({devicePropertyNameSelector + "#boolCtrl", devicePropertyNameSelector + "#doubleArray2DCtrl", devicePropertyNameSelector + "#doubleArrayCtrl", devicePropertyNameSelector + "#floatArray2DCtrl", devicePropertyNameSelector + "#floatArrayCtrl", devicePropertyNameSelector + "#floatCtrl", devicePropertyNameSelector + "#floatCtrl_max", devicePropertyNameSelector + "#floatCtrl_min", devicePropertyNameSelector + "#floatCtrl_units", devicePropertyNameSelector + "#int16Ctrl", devicePropertyNameSelector + "#int16Ctrl_max", devicePropertyNameSelector + "#int16Ctrl_min", devicePropertyNameSelector + "#int16Ctrl_units", devicePropertyNameSelector + "#int32ArrayCtrl", devicePropertyNameSelector + "#int8Array2DCtrl", devicePropertyNameSelector + "#stringArrayCtrl", devicePropertyNameSelector + "#stringCtrl"});
      RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
      rdaDevicePropertyAccess_->get(devicePropertyName,selector,"");

      QQmlPropertyMap * data = rdaDevicePropertyAccess_->getRdaDataAcquired();

      //Wait for data to be filled
      QTest::qSleep(20);

      QMetaType::Type dataType = static_cast<QMetaType::Type>(data->value(devicePropertyNameSelector+ "#floatCtrl").type());

      QVERIFY(data->keys() == dataKeys);
      QVERIFY(dataType == QMetaType::Float);
}


void TestRdaDevicePropertyAccess::setTest()
{
      QString devicePropertyName = "INCA.TESTDEV-01-001-F3/SettingsControl";
      QString fieldName = "floatCtrl";
      QString selector = "PSB.USER.SFTPRO1";
      RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
      rdaDevicePropertyAccess_->get(devicePropertyName,selector,"");

      QQmlPropertyMap * data = rdaDevicePropertyAccess_->getRdaDataAcquired();

      //Wait for data to be filled
      QTest::qSleep(20);

      float newValueToSet = 0 + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(1000)));
      (*data)[devicePropertyName + "#" + fieldName] = newValueToSet;

      rdaDevicePropertyAccess_->set(devicePropertyName,selector);

      rdaDevicePropertyAccess_->get(devicePropertyName,selector,"");
      float fieldValue = data->value(devicePropertyName + "#" + fieldName).toFloat();

      float epsilon =  0.005f;
      QVERIFY( fabsf(fieldValue - newValueToSet) < epsilon);
}

/**
 * @brief TestRdaDevicePropertyAccess::setPartialTest
 *   TODO
 */
void TestRdaDevicePropertyAccess::setPartialTest()
{
      QString devicePropertyName = "INCA.TESTDEV-01-001-F3/Command";
      QString fieldName = "boolCtrl";
      QString selector = "PSB.USER.SFTPRO1";
      RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
      bool exceptionCaught = false;

      try {
          rdaDevicePropertyAccess_->setPartial(devicePropertyName, fieldName, selector, QVariant(true));
      } catch (const std::exception) {
           exceptionCaught = true;
      }

      QCOMPARE(exceptionCaught, false);
}

void TestRdaDevicePropertyAccess::setPartialTestRdaException()
{
      QString devicePropertyName = "INCA.TESTDEV-01-001-F3/SettingsControl";
      QString fieldName = "boolCtrl";
      QString selector = "PSB.USER.SFTPRO1";
      RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);

      QVERIFY_EXCEPTION_THROWN(rdaDevicePropertyAccess_->setPartial(devicePropertyName, fieldName, selector, QVariant(true)),
                                   RdaException);
}

void TestRdaDevicePropertyAccess::fromQtToRdaTest()
{
    RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
    QString deviceName = "INCA.TESTDEV-01-001-F3";
    QString propertyName = "SettingsControl";
    QString fieldName = "floatCtrl";
    QString selector = "PSB.USER.SFTPRO1";
    std::unique_ptr<ClientService>client_ = Rda3Factory::createClientService();
    AccessPoint & accessPoint = client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data());
    // Create RequestContext, which can be used as a selector with a cycle name
    std::auto_ptr <RequestContext> reqContext = RequestContextFactory::create(selector.toLatin1().data());
    // Perform simple GET, remember about auto_ptr, which destroys data when it goes out of scope
    std::auto_ptr < AcquiredData > acqData = accessPoint.get(reqContext);
    // The resulting data is obtained by calling getData() method on the AcquiredData instance
    const Data & data = acqData->getData();

    const Entry * entry = data.getEntry(fieldName.toLatin1().data());

    std::auto_ptr<Data> newData = DataFactory::createData();
    float valueToSet = 2.3f;
    rdaDevicePropertyAccess_->fromQtToRda(entry, deviceName+"/"+propertyName, selector, newData, QVariant(valueToSet));

    float value = newData->getFloat(fieldName.toLatin1().data());

    float epsilon =  0.005f;
    QVERIFY( fabsf(value - valueToSet) < epsilon);

}

void TestRdaDevicePropertyAccess::fromRdaToQtTest()
{
    RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
    QString deviceName = "INCA.TESTDEV-01-001-F3";
    QString propertyName = "SettingsControl";
    QString fieldName = "floatCtrl";
    QString selector = "PSB.USER.SFTPRO1";
    std::unique_ptr<ClientService>client_ = Rda3Factory::createClientService();
    AccessPoint & accessPoint = client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data());
    // Create RequestContext, which can be used as a selector with a cycle name
    std::auto_ptr <RequestContext> reqContext = RequestContextFactory::create(selector.toLatin1().data());
    // Perform simple GET, remember about auto_ptr, which destroys data when it goes out of scope
    std::auto_ptr < AcquiredData > acqData = accessPoint.get(reqContext);
    // The resulting data is obtained by calling getData() method on the AcquiredData instance
    const Data & data = acqData->getData();

    const Entry * entry = data.getEntry(fieldName.toLatin1().data());

    float expectedValue = entry->getFloat();

    QVariant floatValue = rdaDevicePropertyAccess_->fromRdaToQt(entry);

    float epsilon =  0.005f;
    QVERIFY( fabsf(floatValue.toFloat() - expectedValue) < epsilon);
}

void TestRdaDevicePropertyAccess::fromQVariantToRdaTest()
{
    RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
    QString fieldName = "floatCtrl";
    std::auto_ptr<Data> newData = DataFactory::createData();
    bool expectedValue = true;
    rdaDevicePropertyAccess_->fromQVariantToRda(QVariant(expectedValue), fieldName, newData);

    bool value = newData->getBool(fieldName.toLatin1().data());

    QCOMPARE(value, expectedValue);
}

void TestRdaDevicePropertyAccess::parseToQMapTest()
{
    RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
    QString deviceName = "INCA.TESTDEV-01-001-F3";
    QString propertyName = "SettingsControl";
    QString fieldName = "floatCtrl";
    QString selector = "PSB.USER.SFTPRO1";
    QString devicePropertyNameSelector = selector + ":" + deviceName + "/" + propertyName;
    QStringList dataKeys({devicePropertyNameSelector + "#boolCtrl", devicePropertyNameSelector + "#doubleArray2DCtrl", devicePropertyNameSelector + "#doubleArrayCtrl", devicePropertyNameSelector + "#floatArray2DCtrl", devicePropertyNameSelector + "#floatArrayCtrl", devicePropertyNameSelector + "#floatCtrl", devicePropertyNameSelector + "#floatCtrl_max", devicePropertyNameSelector + "#floatCtrl_min", devicePropertyNameSelector + "#floatCtrl_units", devicePropertyNameSelector + "#int16Ctrl", devicePropertyNameSelector + "#int16Ctrl_max", devicePropertyNameSelector + "#int16Ctrl_min", devicePropertyNameSelector + "#int16Ctrl_units", devicePropertyNameSelector + "#int32ArrayCtrl", devicePropertyNameSelector + "#int8Array2DCtrl", devicePropertyNameSelector + "#stringArrayCtrl", devicePropertyNameSelector + "#stringCtrl"});
    std::unique_ptr<ClientService>client_ = Rda3Factory::createClientService();
    AccessPoint & accessPoint = client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data());
    // Create RequestContext, which can be used as a selector with a cycle name
    std::auto_ptr <RequestContext> reqContext = RequestContextFactory::create(selector.toLatin1().data());
    // Perform simple GET, remember about auto_ptr, which destroys data when it goes out of scope
    std::auto_ptr < AcquiredData > acqData = accessPoint.get(reqContext);
    // The resulting data is obtained by calling getData() method on the AcquiredData instance
    const Data & data = acqData->getData();

    std::vector< const Entry * > entries = data.getEntries();

    rdaDevicePropertyAccess_->parseToQMap(entries, deviceName+"/"+propertyName, selector);
    QQmlPropertyMap * dataMap = rdaDevicePropertyAccess_->getRdaDataAcquired();

    QVERIFY(dataMap->keys() == dataKeys);
}

// *****************  qVariantToArray1DTest **************************
void TestRdaDevicePropertyAccess::qVariantToArray1DTestInt()
{
    QVariantList values = {2,3,4,5,7};
    qVariantToArray1DTest<int32_t>(values);
}

void TestRdaDevicePropertyAccess::qVariantToArray1DTestBool()
{
    QVariantList values = {true,false,true,false};
    qVariantToArray1DTest<bool>(values);
}

void TestRdaDevicePropertyAccess::qVariantToArray1DTestFloat()
{
    QVariantList values = {2.3f,3.4f,5.7f,10.3f};
    qVariantToArray1DTest<float>(values);
}

template <typename T>
void TestRdaDevicePropertyAccess::qVariantToArray1DTest(QVariantList values)
{
    RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
    T * valueInt = rdaDevicePropertyAccess_->qVariantToArray1D<T>(values);

    for(QVariantList::iterator i = values.begin(); i != values.end(); ++i)
    {
        QCOMPARE((*i), *valueInt++);
    }
}
// ******************************************************************


// *****************  qVariantToArray2DTest **************************
void TestRdaDevicePropertyAccess::qVariantToArray2DTestInt()
{
    QVariantList values;
    values.append(QVariant::fromValue(QVariantList({1,4,5,6,7})));
    values.append(QVariant::fromValue(QVariantList({10,20,30,40,50})));
    qVariantToArray2DTest<int32_t>(values);
}

void TestRdaDevicePropertyAccess::qVariantToArray2DTestBool()
{
    QVariantList values;
    values.append(QVariant::fromValue(QVariantList({true,false,true,false})));
    values.append(QVariant::fromValue(QVariantList({false,true,false,false})));
    qVariantToArray2DTest<bool>(values);
}

void TestRdaDevicePropertyAccess::qVariantToArray2DTestFloat()
{
    QVariantList values;
    values.append(QVariant::fromValue(QVariantList({2.3f,3.4f,5.7f,10.3f})));
    values.append(QVariant::fromValue(QVariantList({5.3f,8.4f,10.7f,20.3f})));
    qVariantToArray2DTest<float>(values);
}

template <typename T>
void TestRdaDevicePropertyAccess::qVariantToArray2DTest(QVariantList values)
{
    RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
    size_t rows,  columns;
    T * valueInt = rdaDevicePropertyAccess_->qVariantToArray2D<T>(values,rows, columns);

    for(unsigned int i = 0; i < rows; i++)
    {
        for(unsigned int j = 0; j < columns; j++)
        {
                 QCOMPARE(valueInt[i*columns+j], QVariant(values.at(i)).toList().at(j).value<T>());
        }
    }
}
// ******************************************************************


// *****************  array1DToQVariantTest **************************
void TestRdaDevicePropertyAccess::array1DToQVariantTestInt()
{
    const int32_t values[] = {1,3,5,6,7,8};
    array1DToQVariantTest<int32_t>(values);
}

void TestRdaDevicePropertyAccess::array1DToQVariantTestBool()
{
    const bool values[] = {true,false,true,false};
    array1DToQVariantTest<bool>(values);
}

void TestRdaDevicePropertyAccess::array1DToQVariantTestFloat()
{
    const float values[] = {2.3f,3.4f,5.7f,10.3f};
    array1DToQVariantTest<float>(values);
}

template <typename T>
void TestRdaDevicePropertyAccess::array1DToQVariantTest(const T * values)
{
    RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
    size_t size = sizeof(values)/sizeof (T);
    QVariant valueQT = rdaDevicePropertyAccess_->array1DToQVariant<T>(values, size);

    for(unsigned int i = 0; i< size; i++)
    {
        QCOMPARE(values[i], valueQT.toList().at(i).value<T>());
    }
}
// ******************************************************************


// *****************  array2DToQVariantTest *************************
void TestRdaDevicePropertyAccess::array2DToQVariantTestInt()
{
    const int32_t values[] = {1,3,5,6,7,8,9,7,10};
    array2DToQVariantTest<int32_t>(values, 3, 3);
}

void TestRdaDevicePropertyAccess::array2DToQVariantTestBool()
{
    const bool values[] = {true,false,true, false, false, true};
    array2DToQVariantTest<bool>(values, 2, 3);
}

void TestRdaDevicePropertyAccess::array2DToQVariantTestFloat()
{
    const float values[] = {2.3f,3.4f,5.7f,10.3f,100.0f, 10.3f};
    array2DToQVariantTest<float>(values, 2, 4);
}

template <typename T>
void TestRdaDevicePropertyAccess::array2DToQVariantTest(const T * values, size_t rows, size_t columns)
{
    RdaDevicePropertyAccess * rdaDevicePropertyAccess_ = new RdaDevicePropertyAccess(this);
    QVariant valueQT = rdaDevicePropertyAccess_->array2DToQVariant<T>(values, rows, columns);

    for(unsigned int i = 0; i< rows; i++)
    {
        for(unsigned int j = 0; j< columns; j++)
        {
            QCOMPARE(values[i*columns+j], valueQT.toList().at(i).toList().at(j).value<T>());
        }
    }
}
// ***************************************************************

void TestRdaDevicePropertyAccess::cleanupTestCase()
{
     qDebug("called after myFirstTest and mySecondTest");
}

QTEST_MAIN(TestRdaDevicePropertyAccess)
//#include "testrdadevicepropertyaccess.moc"
