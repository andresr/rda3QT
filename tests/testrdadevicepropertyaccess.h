#ifndef TESTRDADEVICEPROPERTYACCESS_H
#define TESTRDADEVICEPROPERTYACCESS_H

#include <QObject>
#include <QtTest/QtTest>
#include <QQmlPropertyMap>
#include <rdadevicepropertyaccess.h>


class TestRdaDevicePropertyAccess : public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();

    void subscriptionTest();
    void unsubscriptionTest();
    void getTest();
    void setTest();
    void setPartialTest();
    void setPartialTestRdaException();

    //From Qt to RDA
    void fromQtToRdaTest();
    void fromQVariantToRdaTest();
    void qVariantToArray1DTestInt();
    void qVariantToArray1DTestBool();
    void qVariantToArray1DTestFloat();
    void qVariantToArray2DTestInt();
    void qVariantToArray2DTestBool();
    void qVariantToArray2DTestFloat();

    //From RDA to Qt
    void fromRdaToQtTest();
    void parseToQMapTest();
    void array1DToQVariantTestInt();
    void array1DToQVariantTestBool();
    void array1DToQVariantTestFloat();
    void array2DToQVariantTestInt();
    void array2DToQVariantTestBool();
    void array2DToQVariantTestFloat();

    void cleanupTestCase();

private:
    template <typename T> void array1DToQVariantTest(const T * values);
    template <typename T> void array2DToQVariantTest(const T * values, size_t rows, size_t columns);
    template <typename T> void qVariantToArray1DTest(QVariantList values);
    template <typename T> void qVariantToArray2DTest(QVariantList values);
};

#endif // TESTRDADEVICEPROPERTYACCESS_H
