#include "rdageneralactionswrapper.h"

RdaGeneralActionsWrapper::RdaGeneralActionsWrapper(QObject *parent):RdaDevicePropertyAccess (parent)
{
    state_ = IDLE;
    this->disconnect(SIGNAL(subscribe(QString, QString, QString)));
    this->disconnect(SIGNAL(unsubscribe(QString)));
}

RdaGeneralActionsWrapper::State RdaGeneralActionsWrapper::getState()
{
    return state_;
}

QString RdaGeneralActionsWrapper::getSelector()
{
    return selector_;
}

void RdaGeneralActionsWrapper::registerDeviceProperty(QString devicePropertyName)
{
    if(!deviceProperties_.contains(devicePropertyName))
    {
        deviceProperties_.append(devicePropertyName);
    }
}

void RdaGeneralActionsWrapper::unsubscribeAll()
{
    if(state_ == RdaGeneralActionsWrapper::State::IDLE)
    {
        QList<QString>::const_iterator it;
        for(it = deviceProperties_.constBegin(); it != deviceProperties_.constEnd(); ++it)
        {
            RdaDevicePropertyAccess::unsubscription((*it), selector_);
        }
    }

}


void RdaGeneralActionsWrapper::subscribeAll()
{
    if(state_ == RdaGeneralActionsWrapper::State::MONITOR)
    {
        QList<QString>::const_iterator it;
        for(it = deviceProperties_.constBegin(); it != deviceProperties_.constEnd(); ++it)
        {
            RdaDevicePropertyAccess::subscription((*it),selector_,"");
        }
    }
}

void RdaGeneralActionsWrapper::getAll()
{
    QList<QString>::const_iterator it;
    for(it = deviceProperties_.constBegin(); it != deviceProperties_.constEnd(); ++it)
    {
        this->get((*it),selector_,"");
    }
}

void RdaGeneralActionsWrapper::restartSubscriptions()
{
    unsubscribeAll();
    subscribeAll();

}

void RdaGeneralActionsWrapper::cleanSubscriptions()
{
    deviceProperties_.clear();
}

void RdaGeneralActionsWrapper::setSelector(QString newSelector)
{
    if (selector_ == newSelector)
       return;

    selector_ = newSelector;

   if(state_ == RdaGeneralActionsWrapper::State::MONITOR)
        unsubscribeAll();

    selectorChanged(selector_);

    if(state_ == RdaGeneralActionsWrapper::State::MONITOR)
         subscribeAll();
}

void RdaGeneralActionsWrapper::setState(const RdaGeneralActionsWrapper::State newState)
{
    if (state_ == newState)
       return;

    state_ = newState;
    emit stateChanged(newState);

    switch (state_) {
        case IDLE:
             unsubscribeAll();
             break;
        case MONITOR:
            subscribeAll();
            break;
        case GET:
            getAll();
            setState(IDLE);
            break;
    }

}
