#include "rda3plugin.h"

void RDA3Plugin::registerTypes(const char *uri)
{
    qmlRegisterType<RdaGeneralActionsWrapper>(uri, 1, 0, "Rda3Qt");
}
