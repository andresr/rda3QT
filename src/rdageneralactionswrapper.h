#ifndef RDAGENERALACTIONSWRAPPER_H
#define RDAGENERALACTIONSWRAPPER_H

#include "rdadevicepropertyaccess.h"
#include <functional>
#include <QObject>


class RdaGeneralActionsWrapper : public RdaDevicePropertyAccess
{
    Q_OBJECT
    Q_PROPERTY(State state READ getState WRITE setState NOTIFY stateChanged)
    Q_PROPERTY(QString selector READ getSelector WRITE setSelector NOTIFY selectorChanged)
    friend class TestGeneralActionsWrapper;
public:
    /**
      * This enumeration represents the different possible states.
     */
    enum State
    {
       /**
       * Does not do anything. (the default state)
       */
       IDLE,
       /**
       * monitor hardware value changes.
       */
       MONITOR,
       /**
       * Do a synchronous get of hardware values.
       */
       GET
    };
    Q_ENUM(State)

    explicit RdaGeneralActionsWrapper(QObject *parent = nullptr);
    State getState();
    QString getSelector();
    Q_INVOKABLE void registerDeviceProperty(QString devicePropertyName);
    Q_INVOKABLE void restartSubscriptions();
    Q_INVOKABLE void cleanSubscriptions();
signals:
    void selectorChanged(QString newSelector);
    void stateChanged(const RdaGeneralActionsWrapper::State newState);

public slots:
    void setSelector(QString newSelector);
    void setState(const RdaGeneralActionsWrapper::State newState);
    void subscription(QString devicePropertyName, QString selector, QString serverName){
        ignore(devicePropertyName);
        ignore(selector);
        ignore(serverName);
        throw std::invalid_argument("Do not use this method in a subclass");
    }
    void unsubscription(QString devicePropertyName){
        ignore(devicePropertyName);
        throw std::invalid_argument("Do not use this method in a subclass");
    }
private:
    void unsubscribeAll();
    void subscribeAll();
    void getAll();
    State state_;
    QString selector_;
    QList<QString> deviceProperties_;
};

#endif // RDAGENERALACTIONSWRAPPER_H
