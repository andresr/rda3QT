 #ifndef RDADEVICEPROPERTYACCESS_H
#define RDADEVICEPROPERTYACCESS_H

#include <QObject>
#include <time.h>
#include <QtCore/qtimer.h>
#include <iostream>
#include <QVariant>
#include <QVector>
#include <qthread.h>
#include <QQmlPropertyMap>
#include <QMetaType>
#include <memory>
#include <QDebug>
#include <cmw-rda3/common/Rda3Factory.h>

using namespace cmw::data;
using namespace cmw::rda3::client;
using namespace cmw::rda3::common;

Q_DECLARE_METATYPE(const int8_t *)
Q_DECLARE_METATYPE(const int16_t *)
Q_DECLARE_METATYPE(const int *)
Q_DECLARE_METATYPE(const long *)
Q_DECLARE_METATYPE(const float *)
Q_DECLARE_METATYPE(float *)
Q_DECLARE_METATYPE(const double *)
Q_DECLARE_METATYPE(const char *)
Q_DECLARE_METATYPE(const char **)

template <typename T>
void ignore(T &&)
{ }

class RdaDevicePropertyAccess : public QObject, public NotificationListener
{
    Q_OBJECT
    Q_PROPERTY(QQmlPropertyMap * rdaDataAcquired READ getRdaDataAcquired WRITE setRdaDataAcquired NOTIFY rdaDataAcquiredChanged)
    Q_PROPERTY(QQmlPropertyMap * errorRdaDataAcquired READ getRdaDataAcquired NOTIFY errorRdaDataAcquiredChanged)
    Q_PROPERTY(QString globalExceptions READ getGlobalExceptions NOTIFY globalExceptionsChanged)

    friend class TestRdaDevicePropertyAccess;
public:
    explicit RdaDevicePropertyAccess(QObject *parent = nullptr);
    void dataReceived(const Subscription & subscription, std::auto_ptr<AcquiredData> acqData, UpdateType updateType);
    void errorReceived(const Subscription & subscription, std::auto_ptr<RdaException> exception, UpdateType updateType);
    QQmlPropertyMap * getRdaDataAcquired() const;
    QQmlPropertyMap * getErrorRdaDataAcquired() const;
    QString getGlobalExceptions() const;
    Q_INVOKABLE void set(QString devicePropertyName, QString selector);
    Q_INVOKABLE void setPartial(QString devicePropertyName, QString field, QString selector, QVariant value);
    Q_INVOKABLE void get(QString devicePropertyName, QString selector, QString serverName);
private:
    QQmlPropertyMap * data_; //Property map with all fields and values of every property
    QQmlPropertyMap * errorData_; //Property map with the error of every device property and field
    QString globalExceptions_; // Variable to expose the global exceptions not related with a single property
    QMap<QString, SubscriptionSharedPtr> subscription_; //Contains all subscriptions to different properties
    std::set<QString> getDoneForProperty_; //It contains the properties which have received a 'get'
    NotificationListenerSharedPtr rdaDevicePropertyAccess_;
    std::unique_ptr<ClientService> client_;

    //From Qt to RDA
    void fromQtToRda(const Entry* entry, QString devicePropertyName, QString selector, std::auto_ptr<Data> &newData, QVariant valueToSet=QVariant());
    void fromQVariantToRda(QVariant value, QString fieldName, std::auto_ptr<Data> &newData);
    template<typename T> T * qVariantToArray1D(QVariant dataValue)
    {
        QVariantList dataMap = dataValue.toList();;
        T * value = new T[dataMap.size()];
        for(int i = 0; i < dataMap.size(); i++)
        {
            T v = dataMap.at(i).value<T>();
            value[i] = v;
        }
        return value;
    }
    template<typename T> T * qVariantToArray2D(QVariant dataValue, size_t &rows, size_t &columns);

    //From RDA to Qt
    QVariant fromRdaToQt(const Entry* entry);
    void parseToQMap(std::vector< const Entry * > entries, QString devicePropertyName_, QString selector);
    template<typename T> QVariant array1DToQVariant(const T * array, size_t size)
    {
        QList<QVariant> list;
        for (unsigned int i=0; i < size;i++) {
            list.append (QVariant::fromValue<T>(array[i]));
        }

        return QVariant::fromValue<QList<QVariant>>(list);
    }
    template<typename T> QVariant array2DToQVariant(const T * array, size_t rowSize, size_t columnSize);
    void setErrorData(QString devicePropertyName, QString field, QString selector, QString error);
signals:
    void subscribe(QString devicePropertyName, QString selector, QString serverName);
    void unsubscribe(QString devicePropertyName, QString selector);
    void rdaDataAcquiredChanged(QQmlPropertyMap * data);
    void errorRdaDataAcquiredChanged(QQmlPropertyMap * data);
    void globalExceptionsChanged(QString exception);

public slots:
    void subscription(QString devicePropertyName, QString selector, QString serverName);
    void unsubscription(QString devicePropertyName, QString selector);
    void setRdaDataAcquired(QQmlPropertyMap * data);
};

#endif // RDADEVICEPROPERTYACCESS_H
