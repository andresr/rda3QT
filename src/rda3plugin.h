#ifndef RDA3PLUGIN_H
#define RDA3PLUGIN_H

#include <QQmlExtensionPlugin>

#include "rdageneralactionswrapper.h"
#include <qqml.h>

class RDA3Plugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)
public:
    void registerTypes(const char *uri);
};

#endif // RDA3PLUGIN_H
