#include "rdadevicepropertyaccess.h"

RdaDevicePropertyAccess::RdaDevicePropertyAccess(QObject *parent) : QObject(parent), rdaDevicePropertyAccess_(this)
{
    connect(this, SIGNAL(subscribe(QString, QString, QString)), this, SLOT(subscription(QString, QString, QString)));
    connect(this, SIGNAL(unsubscribe(QString, QString)), this, SLOT(unsubscription(QString, QString)));

    // First create ClientService and keep it for all further communication
    // Remember that auto_ptr destroys automatically the instance once you get out of scope,
    // therefore you should save ClientService if you want to perform subsequent calls
    client_ = Rda3Factory::createClientService();

    data_ = new QQmlPropertyMap(this);
    errorData_ = new QQmlPropertyMap(this);
}


// SUBSCRIPTION
/**
 * @brief RdaDevicePropertyAccess::subscription
 * It subscribes to the property "devicePropertyName" and we will get the updates in the function "dataReceived"
 * @param devicePropertyName
 * @param selector
 * @param serverName
 */
void RdaDevicePropertyAccess::subscription(QString devicePropertyName, QString selector, QString serverName)
{
    QString devicePropertyNameSelector = selector + ":" + devicePropertyName;
    if (!subscription_.contains(devicePropertyNameSelector))
    {
        QString deviceName = devicePropertyName.section('/',0,0);
        QString propertyName = devicePropertyName.section('/',1,1);
        AccessPoint * accessPoint;
        std::auto_ptr < RequestContext > reqContext ;
        try
        {
            if(!serverName.trimmed().isEmpty())
                accessPoint = &client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data(), serverName.toLatin1().data());
            else
                accessPoint = &client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data());

            reqContext = RequestContextFactory::create(selector.toLatin1().data());
            subscription_.insert(devicePropertyNameSelector,accessPoint->subscribe(reqContext, rdaDevicePropertyAccess_));
            getDoneForProperty_.insert(devicePropertyNameSelector);
        }
            catch (const RdaException & exc)
        {
            globalExceptions_ = devicePropertyName + ": " + exc.what();
            globalExceptionsChanged(globalExceptions_);
           // qDebug() << globalExceptions_;
        }

    }
}

/**
 * @brief RdaDevicePropertyAccess::unsubscription
 * Unsubcribe from the property indicated in "devicePropertyName"
 * @param devicePropertyName
 */
void RdaDevicePropertyAccess::unsubscription(QString devicePropertyName, QString selector)
{
    QString devicePropertyNameSelector = selector + ":" + devicePropertyName;
    if (subscription_.contains(devicePropertyNameSelector))
    {
        subscription_[devicePropertyNameSelector]->unsubscribe();
        subscription_.remove(devicePropertyNameSelector);

        std::set<QString>::iterator it = getDoneForProperty_.find(devicePropertyNameSelector);
        getDoneForProperty_.erase(it);
    }
}


//GET AND SETS
/**
 * @brief RdaDevicePropertyAccess::getRdaDataAcquired
 * @return QQmlPropertyMap with all fields of every subscribed property
 */
QQmlPropertyMap * RdaDevicePropertyAccess::getRdaDataAcquired() const
{
    return data_;
}

QQmlPropertyMap *RdaDevicePropertyAccess::getErrorRdaDataAcquired() const
{
    return errorData_;
}

QString RdaDevicePropertyAccess::getGlobalExceptions() const
{
    return globalExceptions_;
}

/**
 * @brief RdaDevicePropertyAccess::setRdaDataAcquired
 * @param data
 */
void RdaDevicePropertyAccess::setRdaDataAcquired(QQmlPropertyMap* data)
{
    emit rdaDataAcquiredChanged(data);
}

/**
 * @brief RdaDevicePropertyAccess::get
 * Get the values of every field in the property "devicePropertyName" and save them in "data_" variable map.
 * @param devicePropertyName
 * @param selector
 * @param serverName
 */
void RdaDevicePropertyAccess::get(QString devicePropertyName, QString selector, QString serverName)
{
    QString deviceName = devicePropertyName.section('/',0,0);
    QString propertyName = devicePropertyName.section('/',1,1);
    QString devicePropertyNameSelector = selector + ":" + devicePropertyName;
    AccessPoint * accessPoint;
    try
    {
         // AccessPoint replaces rdaDeviceHandle (RDA2), it has similar semantics to JAPC Parameter
        if(!serverName.trimmed().isEmpty())
            accessPoint = &client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data(), serverName.toLatin1().data());
        else
            accessPoint = &client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data());

         std::auto_ptr < RequestContext > reqContext = RequestContextFactory::create(selector.toLatin1().data());
         // Perform simple GET, remember about auto_ptr, which destroys data when it goes out of scope
         std::auto_ptr < AcquiredData > acqData = accessPoint->get(reqContext);

         // The resulting data is obtained by calling getData() method on the AcquiredData instance
          const Data & data = acqData->getData();
          std::vector< const Entry * > entries = data.getEntries();
          //Parsing and set data
          parseToQMap(entries, devicePropertyName, selector);

          setRdaDataAcquired(data_);
          getDoneForProperty_.insert(devicePropertyNameSelector);
    }
    catch (const RdaException & exc)
    {
        globalExceptions_ = devicePropertyName + ": " + exc.what();
        globalExceptionsChanged(globalExceptions_);
       // qDebug() << globalExceptions_;
    }

}

/**
 * @brief RdaDevicePropertyAccess::set
 * It sets all the fields of the property "devicePropertyName" according with the content of "data_" which has been modified in the QML part
 * @param devicePropertyName
 * @param selector
 */
void RdaDevicePropertyAccess::set(QString devicePropertyName, QString selector)
{
    QString devicePropertyNameSelector = selector + ":" + devicePropertyName;
    if(getDoneForProperty_.find(devicePropertyNameSelector) != getDoneForProperty_.end())
    {
        QString deviceName = devicePropertyName.section('/',0,0);
        QString propertyName = devicePropertyName.section('/',1,1);
        // AccessPoint replaces rdaDeviceHandle (RDA2), it has similar semantics to JAPC Parameter
        AccessPoint & accessPoint = client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data());
        // Create RequestContext, which can be used as a selector with a cycle name
        std::auto_ptr <RequestContext> reqContext = RequestContextFactory::create(selector.toLatin1().data());
        // Perform simple GET, remember about auto_ptr, which destroys data when it goes out of scope
        std::auto_ptr < AcquiredData > acqData = accessPoint.get(reqContext);
        // The resulting data is obtained by calling getData() method on the AcquiredData instance
        const Data & data = acqData->getData();
        // Get Entries
        std::vector< const Entry * > entries = data.getEntries();

        // New data
        std::auto_ptr<Data> newData = DataFactory::createData();
         for (std::vector<const Entry* >::iterator it = entries.begin() ; it != entries.end(); ++it)
         {
             try {
                 fromQtToRda((*it), devicePropertyName, selector, newData);
             } catch (std::invalid_argument &e) {
                 setErrorData(devicePropertyName,(*it)->getName(), selector, e.what());
             }
         }

         accessPoint.set(newData,RequestContextFactory::create(selector.toLatin1().data()));
    }
    else
    {
        globalExceptions_ =  "You must do Get before set";
        globalExceptionsChanged(globalExceptions_);
       // qDebug() << globalExceptions_;
    }

}

/**
 * @brief RdaDevicePropertyAccess::setPartial
   TODO: We need to know more information to set write-only properties since we cannot read them. One option is get the CCDA data from a restful api
 * @param devicePropertyName
 * @param field
 * @param selector
 * @param value
 */
void RdaDevicePropertyAccess::setPartial(QString devicePropertyName, QString field, QString selector, QVariant value)
{
    QString deviceName = devicePropertyName.section('/',0,0);
    QString propertyName = devicePropertyName.section('/',1,1);
    // AccessPoint replaces rdaDeviceHandle (RDA2), it has similar semantics to JAPC Parameter
    AccessPoint & accessPoint = client_->getAccessPoint(deviceName.toLatin1().data(), propertyName.toLatin1().data());

    // New data
    std::auto_ptr<Data> newData = DataFactory::createData();
    fromQVariantToRda(value,field, newData);

    accessPoint.set(newData,RequestContextFactory::create(selector.toLatin1().data()));
}

/**
 * @brief RdaDevicePropertyAccess::fromQVariantToRda
 * TODO: It depends of "setPartial"
 * @param value
 * @param fieldName
 * @param newData
 */
void RdaDevicePropertyAccess::fromQVariantToRda(QVariant value, QString fieldName, std::auto_ptr<Data> &newData)
{
    switch (value.type()) {
        case static_cast<QVariant::Type>(QMetaType::Bool):
            newData->append(fieldName.toLatin1().data(), value.toBool());
            return;
         default:
            throw std::invalid_argument("Invalid type of entry " + fieldName.toStdString());
    }

}

/**
 * @brief RdaDevicePropertyAccess::fromQtToRda
 * Get the corresponding value of a QVariant value from "data_" or from the argument "valueToSet" and save it into rda data type "newData"
 * @param entry
 * @param devicePropertyName
 * @param newData
 * @param valueToSet
 */
void RdaDevicePropertyAccess::fromQtToRda(const Entry* entry, QString devicePropertyName, QString selector, std::auto_ptr<Data> &newData,  QVariant valueToSet)
{
    size_t rows, columns;
    const char * fieldName = entry->getName();

    QVariant dataValue = (!valueToSet.isNull())? valueToSet : data_->value(selector + ":" + devicePropertyName+"#"+fieldName);

    switch(entry->getType())
    {
        //////////////////////////////
        // SCALAR
        //////////////////////////////
        case DataType::DT_BOOL:
        {
            bool value = dataValue.toBool();
            newData->append(fieldName, value);
            break;
        }
        case DataType::DT_BYTE:
        {
            int8_t value = static_cast<int8_t>(dataValue.toInt());
            newData->append(fieldName, value);
            break;
        }
        case DataType::DT_SHORT:
        {
            int16_t value = static_cast<int16_t>(dataValue.toInt());
            newData->append(fieldName, value);
            break;
        }
            break;
        case DataType::DT_INT:
        {
            int value = dataValue.toInt();
            newData->append(fieldName, value);
            break;
        }
        case DataType::DT_LONG:
        {
            long value = dataValue.toLongLong();
            newData->append(fieldName, value);
            break;
        }
        case DataType::DT_FLOAT:
        {
            float value = dataValue.toFloat();
            newData->append(fieldName, value);
            break;
        }
        case DataType::DT_DOUBLE:
        {
            double value = dataValue.toDouble();
            newData->append(fieldName, value);
            break;
        }
        case DataType::DT_STRING:
        {
            QVariant valueString = dataValue;
            std::string value = valueString.toString().toStdString();
            newData->append(fieldName, value.c_str());
            break;
        }
        //////////////////////////////
        // ARRAY 1D
        //////////////////////////////
        case DataType::DT_BOOL_ARRAY:
        {
            bool * value = qVariantToArray1D<bool>(dataValue);
            newData->appendArray(fieldName, value, static_cast<size_t>(dataValue.toList().size()));
            break;
        }
        case DataType::DT_BYTE_ARRAY:
        {
            int8_t * value = qVariantToArray1D<int8_t>(dataValue);
            newData->appendArray(fieldName, value, static_cast<size_t>(dataValue.toList().size()));
            break;
        }
        case DataType::DT_SHORT_ARRAY:
        {
            int16_t * value = qVariantToArray1D<int16_t>(dataValue);
            newData->appendArray(fieldName, value, static_cast<size_t>(dataValue.toList().size()));
            break;
        }
        case DataType::DT_INT_ARRAY:
        {
            int * value = qVariantToArray1D<int>(dataValue);
            newData->appendArray(fieldName, value, static_cast<size_t>(dataValue.toList().size()));
            break;
        }
        case DataType::DT_LONG_ARRAY:
        {
            long * value = qVariantToArray1D<long>(dataValue);
            newData->appendArray(fieldName, value, static_cast<size_t>(dataValue.toList().size()));
            break;
        }
        case DataType::DT_FLOAT_ARRAY:
        {
            float * value = qVariantToArray1D<float>(dataValue);
            newData->appendArray(fieldName, value, static_cast<size_t>(dataValue.toList().size()));
            break;
        }
        case DataType::DT_DOUBLE_ARRAY:{
            double * value = qVariantToArray1D<double>(dataValue);
            newData->appendArray(fieldName, value, static_cast<size_t>(dataValue.toList().size()));
            break;
        }
        case DataType::DT_STRING_ARRAY:
        {
            QStringList dataMapList = dataValue.toStringList();
            const char * value[dataMapList.size()];
            for(int i = 0; i < dataMapList.size(); i++)
            {
                value[i] = strdup(dataMapList.at(i).toStdString().c_str());
            }
            newData->appendArray(entry->getName(), value, static_cast<size_t>(dataMapList.size()));
            break;
        }
        /////////////////////////////////////////////////////////////////////
        // ARRAY 2D
        /////////////////////////////////////////////////////////////////////
        case DataType::DT_BOOL_ARRAY_2D:
        {
            bool * value = qVariantToArray2D<bool>(dataValue, rows, columns);
            newData->appendArray2D(fieldName, value, rows , columns);
            break;
        }
        case DataType::DT_BYTE_ARRAY_2D:
        {
            int8_t * value = qVariantToArray2D<int8_t>(dataValue, rows, columns);
            newData->appendArray2D(fieldName, value, rows , columns);
            break;
        }
        case DataType::DT_SHORT_ARRAY_2D:
        {
            int16_t * value = qVariantToArray2D<int16_t>(dataValue, rows, columns);
            newData->appendArray2D(fieldName, value, rows , columns);
            break;
        }
        case DataType::DT_INT_ARRAY_2D:
        {
            int * value = qVariantToArray2D<int>(dataValue, rows, columns);
            newData->appendArray2D(fieldName, value, rows , columns);
            break;
        }
        case DataType::DT_FLOAT_ARRAY_2D:
        {
            float * value = qVariantToArray2D<float>(dataValue, rows, columns);
            newData->appendArray2D(fieldName, value, rows , columns);
            break;
        }
        case DataType::DT_DOUBLE_ARRAY_2D:
        {
            double * value = qVariantToArray2D<double>(dataValue, rows, columns);
            newData->appendArray2D(fieldName, value, rows , columns);
            break;
        }
        case DataType::DT_STRING_ARRAY_2D:
        {
            //TODO
            size_t  rowCount;
            size_t columnCount;
            const char ** originalValue = entry->getArrayString2D(rowCount, columnCount);
            newData->appendArray2D(entry->getName(), originalValue, rowCount, columnCount);
            break;
        }
        default:
            throw std::invalid_argument("Invalid type of entry " + static_cast<std::string>(entry->getName()));
    }


}


template<typename T>
T * RdaDevicePropertyAccess::qVariantToArray2D(QVariant dataValue, size_t &rows, size_t &columns)
{
    QVariantList dataMap = dataValue.toList();
    int rowsI = dataMap.size();
    int columnsI = QVariant(dataMap.at(0)).toList().size();
    int size = rowsI * columnsI;
    T * value = new T[size];
    for(int i = 0; i < rowsI; i++)
    {
       for(int j = 0; j < columnsI; j++)
       {
         T v = QVariant(dataMap.at(i)).toList().at(j).value<T>();
         value[i*columnsI+j] = v;
       }
    }

    rows = static_cast<size_t>(rowsI);
    columns = static_cast<size_t>(columnsI);
    return value;
}

void RdaDevicePropertyAccess::parseToQMap(std::vector< const Entry * > entries, QString devicePropertyName, QString selector)
{
    for (std::vector<const Entry* >::iterator it = entries.begin() ; it != entries.end(); ++it)
    {
        try {
            data_->insert(selector + ":" + devicePropertyName + "#" + (*it)->getName(), fromRdaToQt(*it));
        } catch (std::invalid_argument &e) {
            setErrorData(devicePropertyName,(*it)->getName(), selector, e.what());
        }
    }
}

void RdaDevicePropertyAccess::setErrorData(QString devicePropertyName, QString field, QString selector, QString error)
{
    QString devicePropertyFieldSelector = selector + ":" + devicePropertyName+"#"+field;
    if(errorData_->contains(devicePropertyFieldSelector))
    {
        errorData_->value(devicePropertyFieldSelector).setValue(error);
    }
    else
    {
        errorData_->insert(devicePropertyFieldSelector, error);
    }
    errorRdaDataAcquiredChanged(errorData_);
}

template<typename T>
QVariant RdaDevicePropertyAccess::array2DToQVariant(const T * array, size_t rowSize, size_t columnSize)
{
    QVariantList list;
    for (unsigned long row=0; row < rowSize;row++) {
        QVariantList rowValues;
        for (unsigned long col=0; col < columnSize;col++) {
            rowValues.append(QVariant::fromValue(array[(row*columnSize+col)]));
        }
        list.append(QVariant::fromValue(rowValues));
    }
    return list;
}

/**
 * @brief RdaDevicePropertyAccess::fromRdaToQt
 * Save the RDA value from "entry" in a QVariant
 * @param entry
 * @return  QVariant containing the "entry" value
 */
QVariant RdaDevicePropertyAccess::fromRdaToQt(const Entry* entry)
{
    size_t  rowCount;
    size_t columnCount;
    size_t size;

    switch(entry->getType())
    {
        //////////////////////////////
        // SCALAR
        //////////////////////////////
        case DataType::DT_BOOL:
            return QVariant(entry->getBool());
        case DataType::DT_BYTE:
            return QVariant(entry->getByte());
        case DataType::DT_SHORT:
            return QVariant(entry->getShort());
        case DataType::DT_INT:
            return QVariant(entry->getInt());
        case DataType::DT_LONG:
            return QVariant(static_cast<unsigned long long>(entry->getLong()));
        case DataType::DT_FLOAT:
            return QVariant(entry->getFloat());
        case DataType::DT_DOUBLE:
            return QVariant(entry->getDouble());
        case DataType::DT_STRING:
            return QVariant(entry->getString());
        //////////////////////////////
        // ARRAY 1D
        //////////////////////////////
        case DataType::DT_BOOL_ARRAY:
        {
            const  bool * array= entry->getArrayBool(size);
            return array1DToQVariant<bool>(array, size);
        }
        case DataType::DT_BYTE_ARRAY:
        {
            const  int8_t * array= entry->getArrayByte(size);
            return array1DToQVariant<int8_t>(array, size);
        }
        case DataType::DT_SHORT_ARRAY:
        {
            const  int16_t * array= entry->getArrayShort(size);
            return array1DToQVariant<int16_t>(array, size);
        }
        case DataType::DT_INT_ARRAY:
        {
            const  int * array= entry->getArrayInt(size);
            return array1DToQVariant<int>(array, size);
        }
        case DataType::DT_LONG_ARRAY:
        {
            const  int64_t * array= entry->getArrayLong(size);
            return array1DToQVariant<int64_t>(array, size);
        }
        case DataType::DT_FLOAT_ARRAY:
        {
            const  float * array= entry->getArrayFloat(size);
            return array1DToQVariant<float>(array, size);
        }
        case DataType::DT_DOUBLE_ARRAY:{
            const  double * array= entry->getArrayDouble(size);
            return array1DToQVariant<double>(array, size);
        }
        case DataType::DT_STRING_ARRAY:
        {
            const  char ** array= entry->getArrayString(size);
            QList<QVariant> list;
            for (unsigned int i=0; i < size;i++) {
                list.append (array[i]);
            }
            QVariant qVariant=  QVariant::fromValue<QList<QVariant>>(list);
            return qVariant;
        }
        /////////////////////////////////////////////////////////////////////
        // ARRAY 2D
        /////////////////////////////////////////////////////////////////////
        case DataType::DT_BOOL_ARRAY_2D:
        {
            const  bool * array= entry->getArrayBool2D(rowCount, columnCount);
            return array2DToQVariant<bool>(array, rowCount, columnCount);
        }
        case DataType::DT_BYTE_ARRAY_2D:
        {
            const  int8_t * array= entry->getArrayByte2D(rowCount, columnCount);
            return array2DToQVariant<int8_t>(array, rowCount, columnCount);
        }
        case DataType::DT_SHORT_ARRAY_2D:
        {
            const  int16_t * array= entry->getArrayShort2D(rowCount, columnCount);
            return array2DToQVariant<int16_t>(array, rowCount, columnCount);
        }
        case DataType::DT_INT_ARRAY_2D:
        {
            const  int * array= entry->getArrayInt2D(rowCount, columnCount);
            return array2DToQVariant<int>(array, rowCount, columnCount);
        }
        case DataType::DT_LONG_ARRAY_2D:
        {
            const  int64_t  * array = entry->getArrayLong2D(rowCount, columnCount);
            return array2DToQVariant<int64_t>(array, rowCount, columnCount);
        }
        case DataType::DT_FLOAT_ARRAY_2D:
        {
            const  float * array= entry->getArrayFloat2D(rowCount, columnCount);
            return array2DToQVariant<float>(array, rowCount, columnCount);
        }
        case DataType::DT_DOUBLE_ARRAY_2D:
        {
            const  double * array= entry->getArrayDouble2D(rowCount, columnCount);
            return array2DToQVariant<double>(array, rowCount, columnCount);
        }
        case DataType::DT_STRING_ARRAY_2D:
        {
            const  char ** array= entry->getArrayString2D(rowCount, columnCount);
            QVariantList list;
            for (unsigned long row=0; row < rowCount;row++) {
                QVariantList rows;
                for (unsigned long col=0; col < columnCount;col++) {
                    rows.append(array[(row*columnCount+col)]);
                }
                list.append(QVariant::fromValue(rows));
            }
            return list;
        }
        default:
           throw std::invalid_argument("Invalid type of entry " + static_cast<std::string>(entry->getName()));
    }

}


// RDA LISTENER FUNCTIONS

/**
 * @brief RdaDevicePropertyAccess::dataReceived
 * RDA3 API Method: This method is called when some data is received
 * @param subscription
 * @param acqData
 * @param updateType
 */
void RdaDevicePropertyAccess::dataReceived(const Subscription & subscription, std::auto_ptr<AcquiredData> acqData, UpdateType updateType)
{
    ignore(updateType);
    // AcquiredData contains also AcquiredContext, which provides meta-data about the call,
    // like: cycleName, cycleStamp, acqStamp (similar to JAPC ParameterValueHeader)
    //const AcquiredContext & acqContext = acqData->getContext();

    // Print the acquisition context
   // qDebug() << "Context: " << QString::fromStdString(acqContext.toString());
    // Print type of update: NORMAL, FIRST_UPDATE, IMMEDIATE_UPDATE
   // qDebug()  << "UpdateType: " << QString::fromStdString(cmw::rda3::common::toString(updateType));

    // The data is obtained by calling getData() method on the AcquiredData instance
    const Data & data = acqData->getData();

    std::vector< const Entry * > entries = 	data.getEntries();
    subscription.getAccessPoint().getDeviceName();
    QString devicePropertyName = QString::fromStdString(subscription.getAccessPoint().getDeviceName() + "/" +subscription.getAccessPoint().getPropertyName());
    //Parsing and set data
    parseToQMap(entries, devicePropertyName, QString::fromStdString(subscription.getContext().getSelector()));
    setRdaDataAcquired(data_);
}

/**
 * @brief RdaDevicePropertyAccess::errorReceived
 * RDA3 API Method: This method is called when an error is received
 * @param subscription
 * @param exception
 * @param updateType
 */
void RdaDevicePropertyAccess::errorReceived(const Subscription & subscription, std::auto_ptr<RdaException> exception, UpdateType updateType)
{
    ignore(updateType);
    QString devicePropertyName = QString::fromStdString(subscription.getAccessPoint().getDeviceName() + "/" +subscription.getAccessPoint().getPropertyName());
    globalExceptions_ = devicePropertyName + ": " + exception->what();
    globalExceptionsChanged(globalExceptions_);
   // qDebug() << "Error received:";
   // qDebug() << exception->what();
}
