This example is made to by executed with RDA3QtBridge library containing the rdaDevicePropertyAccess methods directly. In order to achieve that the library should be compiled with the following code in the rda3plugin.cpp:

void RDA3Plugin::registerTypes(const char *uri)
{
    qmlRegisterType<RdaDevicePropertyAccess>(uri, 1, 0, "RdaDevicePropertyAccess");
}

To use the general methods of RdaGeneralActionsWrapper you have to follow the instructions in the example:qml-general-client-library

To be able to execute the client with the RDA3QtBridge library we must set up the following environment variables:

export LD_LIBRARY_PATH="[PATH_TO_LIBRARY]/lib/RDA3/"
export QML2_IMPORT_PATH="[PATH_TO_LIBRARY]/lib/"
