import QtQuick 2.5
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Extras 1.4


Rectangle {
    id:root
    property alias subscriptionButton: subscriptionButton
    property alias devicePropertyText: devicePropertyTE.text
    property alias fieldText: fieldTE.text
    property alias selectorText: selectorTE.text
    property alias valueText: valueTE.text
    property alias valueTextArea: valueTA
    property alias serverNameText: serverNameTE.text
    property var rdaData: ""
    property alias buttonSet: buttonSetMouseArea
    property alias buttonPartialSet: buttonPartialSetMouseArea
    property alias buttonGet: buttonGetMouseArea

    width: 360
    height: 500

    GridLayout {
        id: selectionPane
        height: 150
        rows: 4
        columns: 2
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.top: parent.top
        anchors.topMargin: 10

        Text {
            id: labelServerName
            y: 1
            text: "RDA3 Server name"
            font.bold: true
        }

        Rectangle {
            x: 150
            y: 1
            color: "#ffffff"
            Layout.fillWidth: true
            border.color: "#cccccc"
            border.width: 1
            width: 350
            height: 20
            TextInput {
                id: serverNameTE
                text: ""
                anchors.rightMargin: 0
                anchors.leftMargin: 5
                verticalAlignment: Text.AlignVCenter
                anchors.fill: parent
                maximumLength: 80
                focus: true
            }
        }

        Text {
            id: labelDeviceProperty
            y: 22
            text: "RDA3 Device/Property"
            font.bold: true
        }

        Rectangle {
            x: 150
            y: 22
            color: "#ffffff"
            Layout.fillWidth: true
            border.color: "#cccccc"
            border.width: 1
            width: 350
            height: 20
            TextInput {
                id: devicePropertyTE
                text: ""
                anchors.leftMargin: 5
                verticalAlignment: Text.AlignVCenter
                anchors.fill: parent
                maximumLength: 80
                focus: true
            }
        }

        Text {
            id: labelfield
            text: "RDA3 Field"
            font.bold: true
        }

        Rectangle {
            x: 150
            color: "#ffffff"
            Layout.fillWidth: true
            border.color: "#cccccc"
            border.width: 1
            width: 350
            height: 20
            TextInput {
                id: fieldTE
                text: ""
                anchors.leftMargin: 5
                verticalAlignment: Text.AlignVCenter
                anchors.fill: parent
                maximumLength: 80
                focus: true
            }
        }

        Text {
            id: labelSelector
            x: 0
            height: 20
            text: "RDA3 Selector"
            font.bold: true
            Layout.fillWidth: false
            Layout.fillHeight: false
        }

        Rectangle {
            x: 150
            color: "#ffffff"
            Layout.fillWidth: true
            border.color: "#cccccc"
            border.width: 1
            width: 350
            height: 20
            TextInput {
                id: selectorTE
                text: ""
                anchors.leftMargin: 5
                verticalAlignment: Text.AlignVCenter
                anchors.fill: parent
                maximumLength: 80
                focus: true
            }
        }
    }



    RowLayout {
            id: subscriptionPane
            x: 30
            width: 200
            height: 60
            anchors.top: selectionPane.bottom
            anchors.topMargin: 20

            Label {
                x: 30
                text: "Subscription is "
                font.pointSize: 10
                font.bold: true
                verticalAlignment: Text.AlignVCenter
            }

            ToggleButton {
                id: subscriptionButton
                x: 115
                checked: false
                width: 50
                height: 50
                text: "Off"
                Layout.maximumHeight: 60
                Layout.maximumWidth: 60
                Layout.fillHeight: false
                Layout.fillWidth: false
            }

            Rectangle {
                id: buttonSet

                width: buttonText.width + 20
                height: 30
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "lightsteelblue" }
                    GradientStop { position: 1.0;
                        color: buttonSet.focus ? "red" : "blue" }
                }

                radius: 5
                antialiasing: true

                Text {
                    id: buttonText
                    text: "Set"
                    anchors.centerIn: parent
                    font.pixelSize: parent.height * .5
                    style: Text.Sunken
                    color: "white"
                    styleColor: "black"
                }

                MouseArea {
                    id: buttonSetMouseArea
                    anchors.fill: parent
                }
            }

            Rectangle {
                id: buttonPartialSet

                width: buttonPartialSetText.width + 20
                height: 30
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "lightsteelblue" }
                    GradientStop { position: 1.0;
                        color: buttonPartialSetText.focus ? "red" : "blue" }
                }

                radius: 5
                antialiasing: true

                Text {
                    id: buttonPartialSetText
                    text: "Partial Set"
                    anchors.centerIn: parent
                    font.pixelSize: parent.height * .5
                    style: Text.Sunken
                    color: "white"
                    styleColor: "black"
                }

                MouseArea {
                    id: buttonPartialSetMouseArea
                    anchors.fill: parent
                }
            }

                Rectangle {
                    id: buttonGet

                    width: buttonGetText.width + 20
                    height: 30
                    gradient: Gradient {
                        GradientStop { position: 0.0; color: "lightsteelblue" }
                        GradientStop { position: 1.0;
                            color: buttonGet.focus ? "red" : "blue" }
                    }

                    radius: 5
                    antialiasing: true

                    Text {
                        id: buttonGetText
                        text: "Get"
                        anchors.centerIn: parent
                        font.pixelSize: parent.height * .5
                        style: Text.Sunken
                        color: "white"
                        styleColor: "black"
                    }

                    MouseArea {
                        id: buttonGetMouseArea
                        anchors.fill: parent
                    }
                }


    }



    GridLayout {
        id: valuesPane
        flow: GridLayout.LeftToRight
        anchors.bottomMargin: 10
        anchors.top: subscriptionPane.bottom
        anchors.topMargin: 10
        anchors.right: parent.right
        anchors.rightMargin: 10
        anchors.left: parent.left
        anchors.leftMargin: 30
        rows: 2
        columns: 2

        Text {
            id: labelValue
            x: 0
            y: 0
            font {
                weight: Font.Bold
            }
            text: "Scalar value"
            verticalAlignment: Text.AlignVCenter
        }

        Rectangle {
            id: valueRect
            y: 0
            x: 100
            width: 450
            height: 20
            color: "#ffffff"
            Layout.fillWidth: true
            border.color: "#CCCCCC"
            border.width: 1
            TextEdit {
                id: valueTE
                text: ""
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignLeft
                anchors.leftMargin: 5
                anchors.fill: parent
            }
        }

        Text {
            id: labelValueHistory
            x: 0
            y: 30
            font {
                weight: Font.Bold
            }
            text: "History"
            Layout.alignment: Qt.AlignLeft | Qt.AlignTop
        }

        Rectangle {
            id: historyRect
            x: 100
            y: 30
            width: 450
            height: 200
            color: "#ffffff"
            Layout.fillHeight: true
            Layout.fillWidth: true
            border.color: "#cccccc"

            Flickable {
                id: flickable
                anchors.rightMargin: 2
                anchors.leftMargin: 2
                anchors.bottomMargin: 2
                anchors.topMargin: 2
                anchors.fill: parent

                TextArea.flickable: TextArea {
                    id: valueTA
                    text: ""
                    leftPadding: 6
                    anchors.fill: parent
                    readOnly: true
                    wrapMode: Text.WordWrap
                }

                ScrollBar.vertical: ScrollBar {
                }
            }
        }


    }

    /////////////////////////////////////////////////////////////////////
    // ARRAY2D
    /////////////////////////////////////////////////////////////////////

    Array2D
    {
        id: array2dComponent
        modelComponent: rdaData
        height: 500
        anchors.top: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 80
        anchors.topMargin: 7
    }

    /////////////////////////////////////////////////////////////////////
    // ARRAY1D
    /////////////////////////////////////////////////////////////////////
    Array1D
    {
        id: array1dComponent
        modelComponent: rdaData
        anchors.top: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.topMargin: 7
        anchors.leftMargin: 80
        height: 500
    }


}
