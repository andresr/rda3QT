TARGET = rda3-plugin-project
QT += qml quick

# Ensure that the application will see the import path for the module:
#   * On Windows, do not build into a debug/release subdirectory.
#   * On OS X, add the plugin files into the bundle.
win32: DESTDIR = ./
osx {
    rda3plugin.files = $$OUT_PWD/RDA3
    rda3plugin.path = Contents/PlugIns
    QMAKE_BUNDLE_DATA += rda3plugin
}

SOURCES += app.cpp
RESOURCES += app.qrc

target.path = build
INSTALLS += target


INCLUDEPATH +=  ../../rda3QT/src/
DEPENDPATH +=   ../../rda3QT/src/

LIBS += -L../../lib/RDA3 -lrda3plugin
