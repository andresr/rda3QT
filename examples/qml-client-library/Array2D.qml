import QtQuick 2.5
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Extras 1.4

Rectangle {
    property var modelComponent: ""
    visible: (typeof modelComponent[0][0] === "undefined" || typeof modelComponent[0][0] === "string") ? false : true

    Grid {
        id: rectangle
        columns: modelComponent.length

        Repeater {
            id: repeaterColumns
            model: modelComponent[0].length
            Repeater {
                id: repeaterRows
                model: rdaData.length
                property int columnsRepeaterIndex: index
                Rectangle {
                    width: 30; height: 30
                    TextField
                    {
                        anchors.centerIn: parent
                        text: modelComponent[index][repeaterRows.columnsRepeaterIndex]
                        font.pointSize: 10
                        onEditingFinished: modelComponent[index][repeaterRows.columnsRepeaterIndex] = text
                    }
                }
            }
        }
    }
}
