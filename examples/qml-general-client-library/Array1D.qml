import QtQuick 2.5
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Extras 1.4

Rectangle {
    property var modelComponent: ""
    visible: (typeof modelComponent != "object" || (typeof modelComponent[0][0] !== "undefined" && typeof modelComponent[0][0] !== "string")) ? false : true
    ListView {
        id: listView
        model: modelComponent
        anchors.fill: parent

        delegate: Row {
            Rectangle {
                color: "transparent"
                width: 30; height: 30
                TextField {
                    anchors.centerIn: parent
                    text: modelData
                    font.pointSize: 10
                    onTextChanged: modelComponent[index] = text
                }
            }

        }
    }
}
