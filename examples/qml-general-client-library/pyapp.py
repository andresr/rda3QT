import sys, os
from PyQt5.QtGui import QGuiApplication
from PyQt5.QtQml import QQmlApplicationEngine
from PyQt5.QtCore import QUrl
def main():
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine(parent=app)
    engine.load(QUrl('{}/pyapp.qml'.format(os.path.dirname(os.path.realpath(__file__)))))
    engine.quit.connect(app.quit)
    rc = app.exec_()
if __name__ == "__main__": main()

