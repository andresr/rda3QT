import QtQuick 2.5
import QtQuick.Window 2.2
import RDA3 1.0
/// EXAMPLES:
/// Dev:  FesaDiagnosticsDev1
/// Prop: Counter or RefFaultAndAlarm
/// Selector: PSB.USER.SFTPRO1
///

Item {
    id: root
    visible: true
    width: 2500
    height: 1500

    //*** IMPORTANT (ADDING THE QT OBJECT)
    Rda3Qt {
          id: rdaDevicePropertyAccess
      }

    MainForm {
        id: userFields
        color: "#e3e0e3"
        border.color: "#00000000"
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        devicePropertyText: "INCA.TESTDEV-01-001-F3/SettingsControl"
        fieldText: "floatArray2DCtrl"
        serverNameText: ""
        selectorText: "PSB.USER.SFTPRO1"

        /////////////////////////////////////////////////////////////////////
        // SCALAR, ARRAY1D and ARRAY2D
        /////////////////////////////////////////////////////////////////////
        valueText: rdaData.toString()
        rdaData: rdaDevicePropertyAccess.rdaDataAcquired[userFields.devicePropertyText + '#' + userFields.fieldText]
        onValueTextChanged: {
            // When the valueText is changed we print the array2D by Javascript
            //userFields.showList();

            // Adding the scalar to the historic
            valueTextArea.insert(0, valueText + "\n")
            valueTextArea.cursorPosition = 0
        }

        /////////////////////////////////////////////////////////////////////
        // PRINT ARRAY2D BY JAVASCRIPT
        /////////////////////////////////////////////////////////////////////
        //        function showList() {
        //            console.info("test:" + rdaData)
        //            for ( var i = 0; i < rdaData.length; ++i ) {
        //                 for ( var j = 0; j < rdaData[0].length; ++j ) {
        //                        console.info("++ "+rdaData[i][j] )
        //                 }
        //            }
        //        }

        // Button to subscribe and unsubscribe to the RDA3 property
        buttonRegister.onClicked: {
            rdaDevicePropertyAccess.setSelector(userFields.selectorText);
            rdaDevicePropertyAccess.registerDeviceProperty(userFields.devicePropertyText)
        }

        subscriptionButton.onCheckedChanged: {
            if (subscriptionButton.checked) {
                subscriptionButton.text = "On"
                rdaDevicePropertyAccess.setState(Rda3Qt.MONITOR)
            } else {
                subscriptionButton.text = "Off"
                rdaDevicePropertyAccess.setState(Rda3Qt.IDLE)
            }
        }

        function isNumeric(n) {
          return !isNaN(parseFloat(n)) && isFinite(n);
        }

       buttonSet.onClicked: {
           //If it is not an array
           var dataToSet;
           if(typeof userFields.rdaData !== "object")
           {
               if (isNumeric(userFields.valueText))
               {
                    dataToSet =  Number(userFields.valueText);
               }
               else
               {
                   dataToSet =  userFields.valueText;
               }
           }
           else
           {
               dataToSet = userFields.rdaData
           }

           rdaDevicePropertyAccess.rdaDataAcquired[userFields.devicePropertyText + '#' + userFields.fieldText] = dataToSet;
           rdaDevicePropertyAccess.set(userFields.devicePropertyText, userFields.selectorText)
       }

       buttonGet.onClicked: {
           rdaDevicePropertyAccess.setState(Rda3Qt.GET)
       }

       buttonPartialSet.onClicked: {
           rdaDevicePropertyAccess.setPartial(userFields.devicePropertyText,userFields.fieldText, userFields.selectorText,userFields.valueText);
       }

   }

}
