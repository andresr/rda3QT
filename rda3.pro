TEMPLATE = lib
CONFIG += plugin c++11
QT += qml quick printsupport

CPU=L866
RDA3_VERSION = 2.1.0
RDA3_PATH = /acc/local/$$CPU/cmw/cmw-rda3-cern/$$RDA3_VERSION

DEFINES += QT_DEPRECATED_WARNINGS

DESTDIR = lib/RDA3
TARGET = rda3plugin

HEADERS += \
    src/rda3plugin.h \
    src/rdadevicepropertyaccess.h \
    src/rdageneralactionswrapper.h

SOURCES += \
    src/rda3plugin.cpp \
    src/rdadevicepropertyaccess.cpp \
    src/rdageneralactionswrapper.cpp

pluginfiles.files += \
    qml.qrc \
    qmldir


DESTPATH=$$PWD/../../

target.path=$$DESTPATH
qmldir.files=$$PWD/qmldir
qmldir.path=$$DESTPATH
INSTALLS += target qmldir

# Copy the qmldir file to the same folder as the plugin binary
cpqmldir.files = qmldir
cpqmldir.path = $$DESTDIR
COPIES += cpqmldir


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

QMAKE_CXXFLAGS += -Wno-deprecated

INCLUDEPATH +=  $$RDA3_PATH/include/
DEPENDPATH +=   $$RDA3_PATH/include/

LIBS += -L$$RDA3_PATH/lib/ \
        -lcmw-rda3-cern \
        -lcmw-data-util \
        -lcmw-rda \
        -lcmw-rbac \
        -lcmw-rda3 \
        -lcmw-directory-client \
        -lcmw-data \
        -lcmw-serializer \
        -lcmw-log \
        -lcmw-util \
        -lomniORB4 \
        -lomnithread \
        -liceutil \
        -lzmq \
        -lboost_1_54_0_chrono \
        -lboost_1_54_0_thread \
        -lboost_1_54_0_system \
        -lboost_1_54_0_atomic \
        -lboost_1_54_0_filesystem \
        -lcurl -lcrypto -lrt

PRE_TARGETDEPS += $$RDA3_PATH/lib/libcmw-rda3-cern.a \
                  $$RDA3_PATH/lib/libcmw-data-util.a \
                  $$RDA3_PATH/lib/libcmw-rda.a \
                  $$RDA3_PATH/lib/libcmw-rbac.a \
                  $$RDA3_PATH/lib/libcmw-rda3.a \
                  $$RDA3_PATH/lib/libcmw-directory-client.a \
                  $$RDA3_PATH/lib/libcmw-data.a \
                  $$RDA3_PATH/lib/libcmw-serializer.a \
                  $$RDA3_PATH/lib/libcmw-log.a \
                  $$RDA3_PATH/lib/libcmw-util.a \
                  $$RDA3_PATH/lib/libomniORB4.a \
                  $$RDA3_PATH/lib/libomnithread.a \
                  $$RDA3_PATH/lib/libiceutil.a \
                  $$RDA3_PATH/lib/libzmq.a \
                  $$RDA3_PATH/lib/libboost_1_54_0_chrono.a \
                  $$RDA3_PATH/lib/libboost_1_54_0_thread.a \
                  $$RDA3_PATH/lib/libboost_1_54_0_system.a \
                  $$RDA3_PATH/lib/libboost_1_54_0_atomic.a \
                  $$RDA3_PATH/lib/libboost_1_54_0_filesystem.a

DISTFILES += \
    qmldir
